from django.contrib.gis.db import models

class Routes(models.Model):
    objects  = models.GeoManager()
    gid      = models.IntegerField(primary_key=True)
    id       = models.IntegerField(null=True, blank=True)
    nom      = models.CharField   (max_length=50, blank=True)
    type     = models.CharField   (max_length=50, blank=True)
    se       = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    the_geom = models.MultiLineStringField(srid=4326)
    class Meta:
        db_table = u'routes'

class Points(models.Model):
    objects   = models.GeoManager()
    gid       = models.IntegerField(primary_key=True)
    data_id   = models.CharField   (max_length=80, blank=True)
    direction = models.CharField   (max_length=80, blank=True)
    the_geom  = models.PointField(srid=4326)
    class Meta:
        db_table = u'points'

class Pks(models.Model):
    objects  = models.GeoManager()
    gid      = models.IntegerField(primary_key=True)
    id       = models.FloatField  (null=True, blank=True)
    nom_voie = models.CharField   (max_length=50, blank=True)
    o_f      = models.CharField   (max_length=50, blank=True)
    km       = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    the_geom = models.PointField(srid=4326)
    class Meta:
        db_table = u'pks'

class Projections(models.Model):
    objects   = models.GeoManager()
    point_gid = models.IntegerField()
    route_gid = models.IntegerField()
    the_geom  = models.LineStringField(srid=4326)
    class Meta:
        db_table = u'projection'

class OrthoProjections(models.Model):
    objects   = models.GeoManager()
    point_gid = models.IntegerField()
    route_gid = models.IntegerField()
    the_geom  = models.LineStringField(srid=4326)
    class Meta:
        db_table = u'orthoprojection'

class OrthoProjectionPoints(models.Model):
    objects   = models.GeoManager()
    point_gid = models.IntegerField()
    route_gid = models.IntegerField()
    the_geom  = models.PointField(srid=4326)
    class Meta:
        db_table = u'orthoprojectionpoint'

class PkDistances(models.Model):
    objects    = models.GeoManager()
    orthopoint = models.ForeignKey(OrthoProjectionPoints)
    refpk      = models.ForeignKey(Pks)
    the_geom   = models.LineStringField(srid=4326)
    distance   = models.FloatField()
    class Meta:
        db_table = u'pkdistances'
